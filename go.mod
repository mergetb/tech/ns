module gitlab.com/mergetb/tech/ns

go 1.12

require (
	github.com/sirupsen/logrus v1.4.1
	golang.org/x/sys v0.0.0-20180905080454-ebe1bf3edb33
)
