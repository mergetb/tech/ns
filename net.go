package ns

import (
	"fmt"
	"os"
	"runtime"
	"syscall"

	"golang.org/x/sys/unix"
)

func NewNetns(nsname string) error {

	netnsRunDir := "/var/run/netns"

	// ensure base netns dir exists
	err := os.MkdirAll(netnsRunDir, 0755)
	if err != nil {
		return fmt.Errorf("failed to ensure base netns dir: %v", err)
	}

	netnsPath := fmt.Sprintf("/var/run/netns/%s", nsname)
	madeMount := false

	// if the netowrk namespace does not exist create it
	if _, err := os.Stat(netnsPath); err != nil {

		runtime.LockOSThread()
		// go back to the default namespace when finished, if we can get back, panic,
		// we're no good stuck inside a mzn network namespace
		defer setDefaultNetNs()
		defer runtime.UnlockOSThread()

		// black magic
		for {

			err := unix.Mount("", netnsRunDir, "none", unix.MS_SHARED|unix.MS_REC, "")
			if err != nil {
				errno, ok := err.(syscall.Errno)
				if !ok || errno != unix.EINVAL || madeMount {
					return fmt.Errorf("netns empty bind mount failed: %v", err)
				}

				err := unix.Mount(netnsRunDir, netnsRunDir, "none",
					unix.MS_BIND|unix.MS_REC, "")
				if err != nil {
					//netns self bind mount failed
				}

				madeMount = true
			} else {
				break
			}

		}

		// create the /var/run/netns entry
		ns, err := os.OpenFile(netnsPath, os.O_RDONLY|os.O_CREATE, 0)
		if err != nil {
			return fmt.Errorf("failed to create netns file: %v", err)
		}
		ns.Close()

		// unshare the current namespace
		err = unix.Unshare(unix.CLONE_NEWNET)
		if err != nil {
			os.RemoveAll(netnsPath)
			return fmt.Errorf("failed to unshare NEWNET: %v", err)
		}

		nspath := fmt.Sprintf("/proc/%d/task/%d/ns/net", os.Getpid(), unix.Gettid())
		err = unix.Mount(nspath, netnsPath, "none", unix.MS_BIND, "")
		if err != nil {
			os.RemoveAll(netnsPath)
			return fmt.Errorf("failed to bindmount netns: %v", err)
		}

	}

	return nil

}

func DestroyNetns(nsname string) error {

	nspath := fmt.Sprintf("/var/run/netns/%s", nsname)
	unix.Unmount(nspath, unix.MNT_DETACH)
	return unix.Unlink(nspath)

}

func setDefaultNetNs() error {

	err := setNsFromPath("/proc/1/ns/net")
	if err != nil {
		return fmt.Errorf("default namespace jump failed, exiting")
	}

	return nil

}

func setNsFromPath(path string) error {

	ns, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("failed to open netns: %v", err)
	}
	defer ns.Close()

	err = unix.Setns(int(ns.Fd()), unix.CLONE_NEWNET)
	if err != nil {
		return fmt.Errorf("failed to jump into netns: %v", err)
	}

	return nil

}
